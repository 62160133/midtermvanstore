/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chonchanok.midterm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Van
 */
public class ItemService {

    private static ArrayList<ListItem> ItemList = null;

    static {
        ItemList = new ArrayList<>();
        ItemList.add(new ListItem("id", "name", "brand", 0.0, 0));
    }

    public static boolean addItem(ListItem item) {
        ItemList.add(item);
        save();
        return true;
    }

    public static boolean delItem(ListItem item) {
        ItemList.remove(item);
        save();
        return true;
    }
    public static ListItem delItem(int index) {
        save();
        return ItemList.remove(index);
    }
    
     public static ListItem getItem(int index) {
        return ItemList.get(index);
    }
     public static  ArrayList<ListItem> getItem() {
        return ItemList;
    }

    public static boolean updateItem(int index, ListItem item) {
       ItemList.set(index, item);
       save();
       return true;
    }
     public static boolean clear() {
        ItemList.removeAll(ItemList);
        save();
        return true;
    }
       public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            file = new File("Van.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(ItemList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ItemService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ItemService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;

        try {
            file = new File("Van.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            ItemList = (ArrayList<ListItem>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ItemService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ItemService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ItemService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    static int sizeArray() {
       return ItemList.size();
    }
}
